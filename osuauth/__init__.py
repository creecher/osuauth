import pysnow

class OSUAuthError(Exception):
    pass

class OSUAuth(object):

    def __init__(self, user, password):
        try:
            # Create a connection to ServiceNow
            self.sn_instance = pysnow.Client(instance='osuitsm', user=user, password=password)
    
            # Define user user table
            self.sn_users = self.sn_instance.resource(api_path='/table/sys_user')
    
            # Get the user's profile
            self.profile = self.sn_users.get(query={'user_name': user}, stream=True).first()
        except Exception:
            raise OSUAuthError("Login failed")

        # Define variables
        self.first_name = self.profile['first_name']
        self.last_name = self.profile['last_name']
        self.email = self.profile['email']

